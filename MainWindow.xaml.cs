﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Oefening_29._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        GeometricFigure figure;
        Random mijnRandom = new Random();


        private void btnVoegVierkantToe_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtAantal.Text) <= 0)
                {
                    throw new Exception("Het aantal kan niet kleiner of gelijk aan 0 zijn.");
                }
                figure = new Vierkant(Convert.ToInt32(txtHoogte.Text), Convert.ToInt32(txtBreedte.Text));
                for (int i = 1; i <= Convert.ToInt32(txtAantal.Text); i++)
                {
                    DoubleAnimation myAnimation = new DoubleAnimation
                    {
                        From = 200,
                        To = 0,
                        Duration = TimeSpan.FromSeconds(2),
                        DecelerationRatio = 1,
                    };
                    System.Windows.Shapes.Rectangle rectangle = new System.Windows.Shapes.Rectangle();
                    rectangle.Width = figure.Breedte;
                    rectangle.Height = figure.Hoogte;
                    rectangle.Fill = RandomBrush();
                    rectangle.Margin = new Thickness(mijnRandom.Next(0, 750), mijnRandom.Next(0, 350), 0, 0);
                    cnvCanvas.Children.Add(rectangle);
                    Storyboard sb = new Storyboard();
                    sb.Children.Add(myAnimation);
                    Storyboard.SetTarget(myAnimation, rectangle);
                    Storyboard.SetTargetProperty(myAnimation, new PropertyPath(Canvas.LeftProperty));
                    sb.Begin();
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Geef een geheel getal in in elk tekstveld.", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnVoegRechthoekToe_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtAantal.Text) <= 0)
                {
                    throw new Exception("Het aantal kan niet kleiner of gelijk aan 0 zijn.");
                }
                figure = new Rechthoek(Convert.ToInt32(txtHoogte.Text), Convert.ToInt32(txtBreedte.Text));
                for (int i = 1; i <= Convert.ToInt32(txtAantal.Text); i++)
                {
                    DoubleAnimation myAnimation = new DoubleAnimation
                    {
                        From = 200,
                        To = 0,
                        Duration = TimeSpan.FromSeconds(2),
                        DecelerationRatio = 1,
                    };
                    System.Windows.Shapes.Rectangle rectangle = new System.Windows.Shapes.Rectangle();
                    rectangle.Width = figure.Breedte;
                    rectangle.Height = figure.Hoogte;
                    rectangle.Fill = RandomBrush();
                    rectangle.Margin = new Thickness(mijnRandom.Next(0, 750), mijnRandom.Next(0, 350), 0, 0);
                    cnvCanvas.Children.Add(rectangle);
                    Storyboard sb = new Storyboard();
                    sb.Children.Add(myAnimation);
                    Storyboard.SetTarget(myAnimation, rectangle);
                    Storyboard.SetTargetProperty(myAnimation, new PropertyPath(Canvas.LeftProperty));
                    sb.Begin();
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Geef een geheel getal in in elk tekstveld.", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnVoegDriehoekToe_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtAantal.Text) <= 0)
                {
                    throw new Exception("Het aantal kan niet kleiner of gelijk aan 0 zijn.");
                }
                figure = new Driehoek(Convert.ToInt32(txtHoogte.Text), Convert.ToInt32(txtBreedte.Text));
                for (int i = 1; i <= Convert.ToInt32(txtAantal.Text); i++)
                {
                    DoubleAnimation myAnimation = new DoubleAnimation
                    {
                        From = 200,
                        To = 0,
                        Duration = TimeSpan.FromSeconds(2),
                        DecelerationRatio = 1,
                    };
                    Polygon driehoek = new Polygon();
                    driehoek.Fill = RandomBrush();
                    driehoek.Points.Add(new System.Windows.Point(figure.Hoogte, figure.Breedte));
                    driehoek.Points.Add(new System.Windows.Point(figure.Breedte / 2, 0));
                    driehoek.Points.Add(new System.Windows.Point(0, figure.Hoogte));
                    driehoek.Margin = new Thickness(mijnRandom.Next(0, 750), mijnRandom.Next(0, 350), 0, 0);
                    cnvCanvas.Children.Add(driehoek);
                    Storyboard sb = new Storyboard();
                    sb.Children.Add(myAnimation);
                    Storyboard.SetTarget(myAnimation, driehoek);
                    Storyboard.SetTargetProperty(myAnimation, new PropertyPath(Canvas.LeftProperty));
                    sb.Begin();
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Geef een geheel getal in in elk tekstveld.", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            cnvCanvas.Children.Clear();
        }

        private Brush RandomBrush()
        {
            Random random = new Random();

            byte red = (byte)random.Next(256);
            byte green = (byte)random.Next(256);
            byte blue = (byte)random.Next(256);

            SolidColorBrush brush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(red, green, blue));
            return brush;
        }
    }
}
