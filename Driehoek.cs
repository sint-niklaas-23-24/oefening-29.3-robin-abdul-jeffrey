﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_29._3
{
    internal class Driehoek : GeometricFigure
    {
        public Driehoek() 
        {

        }
        public Driehoek(int hoogte, int breedte) : base(hoogte, breedte)
        {
            Hoogte = hoogte;
            Breedte = breedte;
        }
        public override int BerekenOppervlakte()
        {
            return Hoogte * Breedte / 2;
        }
    }
}
