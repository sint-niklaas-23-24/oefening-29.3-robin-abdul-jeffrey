﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_29._3
{
    class Vierkant : Rechthoek
    {
        public Vierkant(int hoogte, int breedte) : base(hoogte, breedte)
        {
            if (breedte != hoogte)
            {
                breedte = hoogte;
            }
            Breedte = breedte;
            Hoogte = hoogte;
        }
        public Vierkant(int hoogte)
        {
            Hoogte = hoogte;
            Breedte = hoogte;
        }

        public override int BerekenOppervlakte()
        {
            return base.BerekenOppervlakte();
        }
    }
}
