﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_29._3
{
    class Rechthoek : GeometricFigure
    {
        public Rechthoek()
        {

        }
        public Rechthoek(int hoogte, int breedte) : base(hoogte, breedte)
        {
            Hoogte = hoogte;
            Breedte = breedte;
        }
        public override int BerekenOppervlakte()
        {
            return Hoogte * Breedte;
        }
    }
}
