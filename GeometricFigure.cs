﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_29._3
{
    abstract class GeometricFigure
    {
        private int _hoogte;
        private int _breedte;

        public GeometricFigure()
        {

        }
        public GeometricFigure(int hoogte, int breedte)
        {
            Hoogte = hoogte;
            Breedte = breedte;
        }

        public int Hoogte 
        { 
            get { return _hoogte; } 
            set 
            { 
                if (value <= 0)
                {
                    throw new Exception("De hoogte kan niet kleiner zijn dan 0.");
                }
                else
                {
                    _hoogte = value;
                }
            } 
        } 
        public int Breedte 
        { 
            get { return _breedte;} 
            set 
            {
                if (value <= 0)
                {
                    throw new Exception("De breedte kan niet kleiner zijn dan 0.");
                }
                else
                {
                    _breedte = value;
                }
            } 
        }
        public int Oppervlakte
        {
            get { return BerekenOppervlakte(); }
        }
        public abstract int BerekenOppervlakte();
    }
}
